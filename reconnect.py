import rpc_pb2 as ln
import rpc_pb2_grpc as lnrpc
import codecs
import time
import math

def reconnect(rpc):
    addresses={}
    for n in rpc.DescribeGraph(ln.ChannelGraphRequest()).nodes:
        addresses[n.pub_key] = [ a.addr for a in n.addresses]

    for ch in rpc.ListChannels(ln.ListChannelsRequest()).channels:
        if ch.active:
            continue

        for addr in addresses[ch.remote_pubkey]:
            try:
                print("Connecting to %s@%s" % (ch.remote_pubkey, addr))
                rpc.ConnectPeer(ln.ConnectPeerRequest(
                    addr=ln.LightningAddress(pubkey=ch.remote_pubkey, host=addr),
                    perm=True,
                    ))
            except Exception as e:
                print(e)

import rpc_pb2 as ln
import rpc_pb2_grpc as lnrpc
import codecs
import time
import math

'''
cltvExpiry mismatch means that the chain heights are out of sync, like the node is out of sync
Expiry too soon sounds like the deltas run out the clock on a route

'''

def get_channel(rpc, channel_id):
    for ch in rpc.ListChannels(ln.ListChannelsRequest()).channels:
        if ch.chan_id == channel_id:
            return ch
    raise Exception("Channel %d not found" % channel_id)

def get_channel_info(rpc, channel_id):
    return rpc.GetChanInfo(ln.ChanInfoRequest(chan_id=channel_id))

def get_fee_msat(amt_msat, base_fee_msat, fee_rate_milli_msat):
    return base_fee_msat+math.floor(amt_msat*fee_rate_milli_msat/1000000.)

def rebalance(db, rpc, channel_id):
    print(channel_id)
    ch = get_channel(rpc, channel_id)
    ch_info = get_channel_info(rpc, channel_id)

    print(ch)
    print(ch_info)
    print('--------------')
    start_local = int(ch.local_balance)
    start_remote = int(ch.remote_balance)

    amt=1000
    amt_msat=amt*1000

    routes = rpc.QueryRoutes(ln.QueryRoutesRequest(
        pub_key=ch.remote_pubkey,
        amt=amt,
        num_routes=10,
        final_cltv_delta=144))

    for r in routes.routes:
        print("HOPS", len(r.hops))
        print(r)
        print('?????????????')
    return

    for r in routes.routes:
        if r.hops[0].chan_id == channel_id:
            # Skip channel we want to balance
            continue

        add_fee_msat = get_fee_msat(amt_msat, 2000, 10)
        r.hops.extend([ln.Hop(
            chan_id=channel_id,
            amt_to_forward_msat=amt_msat,
            amt_to_forward=amt,
            expiry=r.hops[0].expiry
            ),])

        f=0
        delta=0
        for x in r.hops:
            delta+=0
            x.expiry+=delta
            x.fee_msat=add_fee_msat
            f+=add_fee_msat

        r.total_fees_msat=f
        r.total_amt_msat=amt_msat + f
        r.total_time_lock+=delta+145

        print(r)
        print('???')
        print(ch)
        print('----')
        out = rpc.SendToRouteSync(ln.SendToRouteRequest(
            payment_hash_string='af869206094a5901c5cb5618eb8ff6fc16a9bd4d9c3cd2549b1b14f39d2e526e',
            routes=[r,]))
        print(out)
        if out.payment_error:
            continue

        print('kanal')
        ch2 = get_channel(rpc, channel_id)
        print(ch2)
        print(ch2.local_balance - start_local)
        print(ch2.remote_balance - start_remote)
        break

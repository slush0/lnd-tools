Mixed scripts and tools for making life with LND easier.

Collecting stats:
-----------------

Run `lndtools.py collect` in crontab:

```
*/10 * * * * python3 lndtools.py collect
```

Database schema:
----------------

See `schema.txt`.

Querying database:
------------------

Showing satoshis forwarded over your node (amount in, amount out, total fee):

```
echo "SELECT SUM(amt_in), SUM(amt_out), SUM(fee) FROM fwdinghistory" | sqlite3 lndstats.sqlite
```

Showing nodes with number of channels:

```
SELECT node_pub, alias, COUNT(channel_id) AS cnt
FROM describegraph_channels ch
LEFT JOIN describegraph_nodes n ON ch.node_pub=n.pub_key
GROUP BY node_pub
ORDER BY cnt;
```

Showing forwarding events with aliases of nodes:

```
SELECT DATETIME(fwd.timestamp, 'unixepoch') date, fwd.fee, fwd.amt_in,
ch1.remote_alias, fwd.chan_id_in AS chan_in,
fwd.amt_out, ch2.remote_alias, fwd.chan_id_out AS chan_out
FROM fwdinghistory fwd
LEFT JOIN channels ch1 ON fwd.chan_id_in=ch1.chan_id
LEFT JOIN channels ch2 ON fwd.chan_id_out=ch2.chan_id
ORDER BY fwd.timestamp;
```

Showing channels with aliases and balance index (50=in balance), ordered by total satoshis sent over channel:

```
SELECT ch.extra_is_mine, ch.active, CAST((ch.local_balance / (CAST (ch.capacity AS FLOAT)) * 100) AS INTEGER) AS balance_index, ch.capacity,
ch.local_balance, ch.remote_balance,
ch.total_satoshis_sent, ch.total_satoshis_received, remote_alias, ch.remote_pubkey, ch.chan_id
FROM channels ch
ORDER BY ch.total_satoshis_sent+ch.total_satoshis_received;
```

The same request, ordered by how the channels are balanced:


```
SELECT ch.extra_is_mine, ch.active, CAST((ch.local_balance / (CAST (ch.capacity AS FLOAT)) * 100) AS INTEGER) AS balance_index, ch.capacity,
ch.local_balance, ch.remote_balance,
ch.total_satoshis_sent, ch.total_satoshis_received, remote_alias, ch.remote_pubkey, ch.chan_id
FROM channels ch
ORDER BY ABS(balance_index-50) DESC;
```

Show channels sorted by date of activity (closed channels are filtered out):
```
SELECT DATETIME(ch.extra_last_update, 'unixepoch'), ch.extra_is_mine, ch.active, ch.capacity, ch.local_balance, ch.remote_balance,
ch.total_satoshis_sent, ch.total_satoshis_received, remote_alias, ch.remote_pubkey, ch.chan_id
FROM channels ch
WHERE ch.active > -1
ORDER BY ch.extra_last_update DESC;
```

Show channels inactive for more than 24 hours:

```
SELECT remote_pubkey, remote_alias, DATETIME(extra_last_update, 'unixepoch')
FROM channels WHERE active=0 AND extra_last_update<strftime('%s', CURRENT_TIMESTAMP)-3600*24;
```

import time
import math
import os

from mako.lookup import TemplateLookup
from mako.template import Template
    
lookup = TemplateLookup(directories=['templates'], output_encoding='utf-8', encoding_errors='replace')

def renderer(args):
    def inner(template, **kwargs):
        if not os.path.exists(args.out_dir):
            os.makedirs(args.out_dir)
        out = lookup.get_template(template).render(**kwargs)
        f = open(os.path.join(args.out_dir, template), 'wb')
        f.write(out)
        f.close()
        return out
    return inner

def fwdinghistory(db, args, render):
    print("Generating fwdinghistory.html")
    sql = '''SELECT fwd.timestamp,
                    fwd.fee,
                    fwd.amt_in,
                    ch1.remote_alias as alias_in,
                    fwd.chan_id_in AS chan_in,
                    fwd.amt_out,
                    ch2.remote_alias as alias_out,
                    fwd.chan_id_out AS chan_out
        FROM fwdinghistory fwd
        LEFT JOIN channels ch1 ON fwd.chan_id_in=ch1.chan_id
        LEFT JOIN channels ch2 ON fwd.chan_id_out=ch2.chan_id
        ORDER BY fwd.timestamp DESC
        LIMIT 100'''

    fwdinghistory = db.execute(sql)
    render('fwdinghistory.html', data=fwdinghistory, title='Forwarding history')

def htmlstats(db, args):
    print("Generating HTML stats...")

    render = renderer(args)
    menu = []
    if not args.skip_fwdinghistory:
        fwdinghistory(db, args, render)
        menu.append(('fwdinghistory.html', 'Forwarding history'))

    render('index.html', menu=menu, title='Main index')

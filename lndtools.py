#!/usr/bin/env python3
import argparse
import time
import datetime
import os

import helpers
import database

# Default values, override them from commandline!
CERT_FILE = '~/.lnd/tls.cert'
MACAROON_FILE = '~/.lnd/data/chain/bitcoin/mainnet/admin.macaroon'
GRPC_URL = 'localhost:10009'
#REST_URL = 'https://localhost:10010'
DB_FILE = 'lndstats.sqlite'

def main():
    parser = argparse.ArgumentParser(description='Mixed tools for LND', formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('-m', '--macaroon', help='Path to admin macaroon', default=MACAROON_FILE)
    parser.add_argument('-c', '--cert', help='Path to SSL cert', default=CERT_FILE)
    parser.add_argument('-g', '--grpc-url', help='gRPC endpoint URL', default=GRPC_URL)
    # parser.add_argument('-r', '--rest-url', help='REST endpoint URL', default=REST_URL)
    parser.add_argument('-d', '--database', help='Path to sqlite file', default=DB_FILE)
    parser.add_argument('-b', '--backup', help='Perform backup of sqlite file', action='store_true')
    subparsers = parser.add_subparsers(dest='command', title='Commands to execute')

    parser_collect = subparsers.add_parser('collect', help='Collect data from lnd (run this as a cron job)')
    parser_reconnect = subparsers.add_parser('reconnect', help='Reconnect to all peers')

    parser_rebalance = subparsers.add_parser('rebalance', help='Rebalance specific channel')
    parser_rebalance.add_argument('channel_id', type=int, help='Channel to rebalance')
    parser_rebalance.add_argument('-o', '--other_channel_id', type=int)

    parser_closechannels = subparsers.add_parser('closechannels', help='Close channels by various metrics')
    parser_closechannels.add_argument('-z', '--close_zombies', action='store_true')

    parser_htmlstats = subparsers.add_parser('htmlstats', help='Render statistics as HTML')
    parser_htmlstats.add_argument('-o', '--out_dir', help='Output directory', default='./htmlstats/')
    parser_htmlstats.add_argument('-sf', '--skip_fwdinghistory', action='store_true')


    args = parser.parse_args()

    if args.command is None:
        parser.print_help()
        parser.exit()
    
    args.database = os.path.expanduser(args.database)
    
    start = int(time.time())
    print("Starting at", datetime.datetime.now())

    print("Loading database %s..." % args.database)
    db = database.db_open(args.database, args.backup)
    start_size = os.path.getsize(args.database)

    print("Connecting to lnd at %s..." % args.grpc_url)
    rpc = helpers.grpc_authorize(args.cert, args.macaroon, args.grpc_url)

    if args.command == 'collect':
        import collect
        collect.collect(args, db, rpc)

    elif args.command == 'reconnect':
        import reconnect
        reconnect.reconnect(rpc)

    elif args.command == 'closechannels':
        import closechannels
        closechannels.closechannels(args, db, rpc)

    elif args.command == 'rebalance':
        import rebalance
        rebalance.rebalance(db, rpc, args.channel_id)

    elif args.command == 'htmlstats':
        import htmlstats
        htmlstats.htmlstats(db, args)

    else:
        parser.error("Unknown command")

    db.close()
    if os.path.getsize(args.database) - start_size > 0:
        print("Database size increase: %d bytes" % (os.path.getsize(args.database) - start_size))
    print("Total run time: %.02f sec" % (time.time() - start))

if __name__ == '__main__':
    main()

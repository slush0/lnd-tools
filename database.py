import sqlite3
import os
import time
import shutil
import gzip

DB_VERSION_LATEST = 4

def db_open(db_file, backup=True):
    is_new = not os.path.exists(db_file)

    if backup and not is_new:
        with open(db_file, 'rb') as f_in:
            with gzip.open('%s.bak-%d.gz' % (db_file, int(time.time())), 'wb') as f_out:
                shutil.copyfileobj(f_in, f_out)

    conn = sqlite3.connect(db_file, isolation_level=None)
    conn.row_factory = sqlite3.Row
    if is_new:
        migration_1(conn)
    db_version = conn.execute('''SELECT value_int as value FROM config WHERE key='db_version';''').fetchone()['value']

    if db_version < DB_VERSION_LATEST:
        # Perform database migrations
        for v in range(db_version+1, DB_VERSION_LATEST+1):
            __import__(__name__).__getattribute__('migration_%d' % v)(conn)

    conn.execute('PRAGMA JOURNAL_MODE=WAL')
    conn.execute('PRAGMA SYNCHRONOUS=0')
    return conn


def migration_1(conn):
    # Initial scheme creation
    print('Initializing database...')
    conn.execute('''CREATE TABLE config (key TEXT UNIQUE, value_str TEXT, value_int INTEGER)''')
    conn.execute('''INSERT INTO config (key, value_int) VALUES ('db_version', 1)''')
    conn.commit()


def migration_2(conn):
    print('Updating database...')
    conn.execute('''CREATE TABLE network_info (
                        timestamp INTEGER PRIMARY KEY,
                        graph_diameter INTEGER,
                        avg_out_degree REAL,
                        max_out_degree INTEGER,
                        num_nodes INTEGER,
                        num_channels INTEGER,
                        total_network_capacity INTEGER,
                        avg_channel_size REAL,
                        min_channel_size INTEGER,
                        max_channel_size INTEGER)''')
    conn.execute('''CREATE TABLE describegraph_nodes (
                        pub_key TEXT PRIMARY KEY,
                        last_update INTEGER,
                        alias TEXT,
                        addresses TEXT,
                        color TEXT,
                        extra_first_seen INTEGER,
                        extra_uptime_counter INTEGER)''')
    conn.execute('''CREATE TABLE describegraph_channels (
                        channel_id INTEGER,
                        node_pub TEXT,
                        chan_point TEXT,
                        last_update INTEGER,
                        capacity INTEGER,
                        node_policy_time_lock_delta INTEGER,
                        node_policy_min_htlc INTGER,
                        node_policy_fee_base_msat INTEGER,
                        node_policy_fee_rate_milli_msat INTEGER,
                        extra_created INTEGER,
                        PRIMARY KEY (channel_id, node_pub),
                        CONSTRAINT node_pub_fk FOREIGN KEY (node_pub)
                            REFERENCES describegraph_nodes(pub_key))''')
    conn.execute('''CREATE TABLE describegraph_channels_policy_history (
                        channel_id INTEGER,
                        node_pub TEXT NOT NULL,
                        timestamp INTEGER,
                        node_policy_time_lock_delta INTEGER,
                        node_policy_min_htlc INTEGER,
                        node_policy_fee_base_msat INTEGER,
                        node_policy_fee_rate_milli_msat INTEGER,
                        CONSTRAINT channel_id_node_pub_fk FOREIGN KEY (channel_id, node_pub)
                            REFERENCES describegraph_channels(channel_id, node_pub))''')
    conn.execute('''CREATE TABLE channels (
                        channel_point TEXT PRIMARY KEY,
                        chan_id INTEGER,
                        active INTEGER,
                        remote_pubkey TEXT NOT NULL,
                        capacity INTEGER,
                        local_balance INTEGER,
                        remote_balance INTEGER,
                        commit_fee INTEGER,
                        commit_weight INTEGER,
                        fee_per_kw INTEGER,
                        unsettled_balance INTEGER,
                        total_satoshis_sent INTEGER,
                        total_satoshis_received INTEGER,
                        num_updates INTEGER,
                        pending_htlcs TEXT,
                        csv_delay INTEGER,
                        private INTEGER,
                        close_tx_hash TEXT,
                        close_height INTEGER,
                        close_settled_balance INTEGER,
                        close_time_locked_balance INTEGER,
                        close_type TEXT,
                        close_timestamp INTEGER,
                        extra_is_mine INTEGER,
                        extra_created INTEGER,
                        extra_last_update INTEGER,
                        CONSTRAINT remote_pubkey_fk FOREIGN KEY (remote_pubkey)
                            REFERENCES describegraph_nodes(pub_key))''')
    conn.execute('''CREATE TABLE channels_history (
                        channel_point TEXT NOT NULL,
                        timestamp INTEGER,
                        active INTEGER,
                        local_balance INTEGER,
                        remote_balance INTEGER,
                        total_satoshis_sent INTEGER,
                        total_satoshis_received INTEGER,
                        commit_fee INTEGER,
                        CONSTRAINT chanel_point_fk FOREIGN KEY (channel_point) REFERENCES channels(channel_point))''')
    conn.execute('''CREATE TABLE fwdinghistory (
                        chan_id_in INTEGER NOT NULL,
                        chan_id_out INTEGER NOT NULL,
                        timestamp INTEGER,
                        amt_in INTEGER,
                        amt_out INTEGER,
                        fee INTEGER,
                        CONSTRAINT chan_id_in_fk FOREIGN KEY (chan_id_in) REFERENCES channels(chan_id),
                        CONSTRAINT chan_id_out_fk FOREIGN KEY (chan_id_out) REFERENCES channels(chan_id))''')
    conn.execute('''UPDATE config SET value_int=2 WHERE key='db_version';''')
    conn.commit()

def migration_3(conn):
    print("Updating database...")
    conn.execute("ALTER TABLE channels ADD COLUMN remote_alias TEXT")
    conn.execute("UPDATE channels SET remote_alias = (SELECT alias FROM describegraph_nodes dn WHERE dn.pub_key = channels.remote_pubkey)")
    conn.execute("UPDATE channels SET remote_alias = substr(remote_pubkey, 0, 16) WHERE remote_alias = ''")
    conn.execute("UPDATE config SET value_int=3 WHERE key='db_version'")
    conn.commit()

def migration_4(conn):
    print("Updating database...")
    conn.execute('''CREATE TABLE balances (
                        timestamp INTEGER PRIMARY KEY,
                        wallet_total INTEGER,
                        wallet_confirmed INTEGER,
                        wallet_unconfirmed INTEGER,
                        channel_total INTEGER,
                        channel_pending INTEGER)''')
    conn.execute("UPDATE config SET value_int=4 WHERE key='db_version'")
    conn.commit()

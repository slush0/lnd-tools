CREATE TABLE config (key TEXT UNIQUE, value_str TEXT, value_int INTEGER)
CREATE TABLE network_info (
                        timestamp INTEGER PRIMARY KEY,
                        graph_diameter INTEGER,
                        avg_out_degree REAL,
                        max_out_degree INTEGER,
                        num_nodes INTEGER,
                        num_channels INTEGER,
                        total_network_capacity INTEGER,
                        avg_channel_size REAL,
                        min_channel_size INTEGER,
                        max_channel_size INTEGER)
CREATE TABLE describegraph_nodes (
                        pub_key TEXT PRIMARY KEY,
                        last_update INTEGER,
                        alias TEXT,
                        addresses TEXT,
                        color TEXT,
                        extra_first_seen INTEGER,
                        extra_uptime_counter INTEGER)
CREATE TABLE describegraph_channels (
                        channel_id INTEGER,
                        node_pub TEXT,
                        chan_point TEXT,
                        last_update INTEGER,
                        capacity INTEGER,
                        node_policy_time_lock_delta INTEGER,
                        node_policy_min_htlc INTGER,
                        node_policy_fee_base_msat INTEGER,
                        node_policy_fee_rate_milli_msat INTEGER,
                        extra_created INTEGER,
                        PRIMARY KEY (channel_id, node_pub),
                        CONSTRAINT node_pub_fk FOREIGN KEY (node_pub)
                            REFERENCES describegraph_nodes(pub_key))
CREATE TABLE describegraph_channels_policy_history (
                        channel_id INTEGER,
                        node_pub TEXT NOT NULL,
                        timestamp INTEGER,
                        node_policy_time_lock_delta INTEGER,
                        node_policy_min_htlc INTEGER,
                        node_policy_fee_base_msat INTEGER,
                        node_policy_fee_rate_milli_msat INTEGER,
                        CONSTRAINT channel_id_node_pub_fk FOREIGN KEY (channel_id, node_pub)
                            REFERENCES describegraph_channels(channel_id, node_pub))
CREATE TABLE channels (
                        channel_point TEXT PRIMARY KEY,
                        chan_id INTEGER,
                        active INTEGER,
                        remote_pubkey TEXT NOT NULL,
                        capacity INTEGER,
                        local_balance INTEGER,
                        remote_balance INTEGER,
                        commit_fee INTEGER,
                        commit_weight INTEGER,
                        fee_per_kw INTEGER,
                        unsettled_balance INTEGER,
                        total_satoshis_sent INTEGER,
                        total_satoshis_received INTEGER,
                        num_updates INTEGER,
                        pending_htlcs TEXT,
                        csv_delay INTEGER,
                        private INTEGER,
                        close_chain_hash TEXT,
                        close_tx_hash TEXT,
                        close_height INTEGER,
                        close_settled_balance INTEGER,
                        close_time_locked_balance INTEGER,
                        close_type TEXT,
                        close_timestamp INTEGER,
                        extra_is_mine INTEGER,
                        extra_created INTEGER,
                        extra_last_update INTEGER, remote_alias TEXT,
                        CONSTRAINT remote_pubkey_fk FOREIGN KEY (remote_pubkey)
                            REFERENCES describegraph_nodes(pub_key))
CREATE TABLE channels_history (
                        channel_point TEXT NOT NULL,
                        timestamp INTEGER,
                        active INTEGER,
                        local_balance INTEGER,
                        remote_balance INTEGER,
                        total_satoshis_sent INTEGER,
                        total_satoshis_received INTEGER,
                        commit_fee INTEGER,
                        CONSTRAINT chanel_point_fk FOREIGN KEY (channel_point) REFERENCES channels(channel_point))
CREATE TABLE fwdinghistory (
                        chan_id_in INTEGER NOT NULL,
                        chan_id_out INTEGER NOT NULL,
                        timestamp INTEGER,
                        amt_in INTEGER,
                        amt_out INTEGER,
                        fee INTEGER,
                        CONSTRAINT chan_id_in_fk FOREIGN KEY (chan_id_in) REFERENCES channels(chan_id),
                        CONSTRAINT chan_id_out_fk FOREIGN KEY (chan_id_out) REFERENCES channels(chan_id))

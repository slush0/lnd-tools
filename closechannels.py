import time
import json

import rpc_pb2 as ln
import rpc_pb2_grpc as lnrpc

def close_zombies(db, rpc):
    sql = '''SELECT channel_point, remote_pubkey, local_balance, remote_balance FROM channels WHERE active=0 AND extra_last_update<strftime('%s', CURRENT_TIMESTAMP)-3600*24*30 AND private=0 limit 50'''

    for row in db.execute(sql):
        tx_hash, tx_index = row['channel_point'].split(':')
        print("Closing channel", tx_hash, tx_index, "pubkey", row['remote_pubkey'], "local balance", row['local_balance'], "remote balance", row['remote_balance'])

        channel_point = ln.ChannelPoint(
                funding_txid_str = tx_hash,
                output_index = int(tx_index)
        )

        try:
            for x in rpc.CloseChannel(ln.CloseChannelRequest(channel_point=channel_point, force=True)):
                print(x)
                break
        except Exception as e:
            print(e)

def closechannels(args, db, rpc):
    if args.close_zombies:
        close_zombies(db, rpc)

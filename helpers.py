import os
import codecs
import json
import base64
import requests

def grpc_metadata_callback(macaroon_file):
    def inner_callback(context, callback):
        # for more info see grpc docs
        macaroon_bytes = open(os.path.expanduser(macaroon_file), 'rb').read()
        macaroon = codecs.encode(macaroon_bytes, 'hex')
        callback([('macaroon', macaroon)], None)
    return inner_callback

class RestApi(object):
    def __init__(self, cert_file, macaroon_file, rest_url):
        self.rest_url = rest_url
        self.cert_file = os.path.expanduser(cert_file)
        self.macaroon_file = os.path.expanduser(macaroon_file)

    def call(self, path):
        url = '%s%s' % (self.rest_url, path)
        macaroon = codecs.encode(open(self.macaroon_file, 'rb').read(), 'hex')
        headers = {'Grpc-Metadata-macaroon': macaroon}
        r = requests.get(url, headers=headers, verify=self.cert_file)
        return r.json()

def grpc_authorize(cert_file, macaroon_file, grpc_url):
    import grpc
    import rpc_pb2_grpc as lnrpc

    # Due to updated ECDSA generated tls.cert we need to let gprc know that
    # we need to use that cipher suite otherwise there will be a handhsake
    # error when we communicate with the lnd rpc server.
    os.environ["GRPC_SSL_CIPHER_SUITES"] = 'HIGH+ECDSA'

    cert = open(os.path.expanduser(cert_file), 'rb').read()
    cert_creds = grpc.ssl_channel_credentials(cert)
    auth_creds = grpc.metadata_call_credentials(grpc_metadata_callback(macaroon_file))
    combined_creds = grpc.composite_channel_credentials(cert_creds, auth_creds)
    channel = grpc.secure_channel(grpc_url, combined_creds, options=[
                ('grpc.max_send_message_length', 50 * 1024 * 1024),
                ('grpc.max_receive_message_length', 50 * 1024 * 1024)
        ])
    return lnrpc.LightningStub(channel)

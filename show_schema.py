#!/usr/bin/env python3
import time
import datetime
import json
import sys
import os

import database
from config import *

def main():
    if len(sys.argv) > 1:
        db_file = os.path.expanduser(sys.argv[1])
    else:
        db_file = os.path.expanduser(DB_FILE)

    db = database.db_open(db_file, DB_BACKUP)
    for row in db.execute("SELECT * FROM sqlite_master WHERE type='table'"):
        print(row['sql'])

if __name__ == '__main__':
    main()

import time
import json

import rpc_pb2 as ln
import rpc_pb2_grpc as lnrpc
from google.protobuf.json_format import MessageToDict

UPTIME_COUNTER_DELTA = 3*3600

def lookup_tx(channel_point, transactions):
    for tx in transactions.transactions:
        if tx.tx_hash == channel_point.split(':')[0]:
            return (True, tx.time_stamp)

    # FIXME Lookup to block explorer
    return (False, 0)

def update_networkinfo(db, ni, timestamp):
    print("Updating network_info...")
    sql = '''INSERT INTO network_info (timestamp, graph_diameter, avg_out_degree,
                max_out_degree, num_nodes, num_channels, total_network_capacity,
                avg_channel_size, min_channel_size, max_channel_size) VALUES
                (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)'''

    db.execute(sql, [timestamp, ni.graph_diameter, ni.avg_out_degree,
            ni.max_out_degree, ni.num_nodes, ni.num_channels, ni.total_network_capacity,
            ni.avg_channel_size, ni.min_channel_size, ni.max_channel_size])
    db.commit()

def update_describegraph_nodes(db, dg, timestamp):
    print("Updating describegraph_nodes...")

    print("Fetching nodes...")
    known = []
    for row in db.execute('SELECT pub_key FROM describegraph_nodes'):
        known.append(row['pub_key'])

    sql_insert = '''INSERT INTO describegraph_nodes (pub_key, last_update, alias,
                addresses, color, extra_first_seen, extra_uptime_counter)
                VALUES (?, ?, ?, ?, ?, ?, ?)'''

    sql_update = '''UPDATE describegraph_nodes SET last_update=?, alias=?,
                addresses=?, color=?, extra_uptime_counter=extra_uptime_counter+?
                WHERE pub_key=?'''

    to_insert = []
    to_update = []
    for node in dg.nodes:
        addresses = json.dumps(MessageToDict(node).get('addresses'), [])
        alias = node.alias or node.pub_key[:10]

        if node.pub_key in known:
            if timestamp - node.last_update < UPTIME_COUNTER_DELTA:
                uptime_counter_delta = 1
            else:
                uptime_counter_delta = 0
            to_update.append((node.last_update, node.alias, addresses, node.color, uptime_counter_delta, node.pub_key))
        else:
            to_insert.append((node.pub_key, node.last_update, node.alias, addresses, node.color, node.last_update, 0))

    print("New %d nodes..." % len(to_insert))
    db.executemany(sql_insert, to_insert)
    print("Updating %d nodes..." % len(to_update))
    db.executemany(sql_update, to_update)
    db.commit()

def update_describegraph_channels(db, dg, timestamp):
    print("Updating describegraph_channels...")

    print("Fetching channels...")
    known = {}
    for row in db.execute('''SELECT channel_id, node_pub, node_policy_time_lock_delta, node_policy_min_htlc,
                node_policy_fee_base_msat, node_policy_fee_rate_milli_msat FROM describegraph_channels'''):
        known[(row['channel_id'], row['node_pub'])] = (row['node_policy_time_lock_delta'], row['node_policy_min_htlc'],
                row['node_policy_fee_base_msat'], row['node_policy_fee_rate_milli_msat'])

    sql_insert = '''INSERT INTO describegraph_channels (channel_id, node_pub, chan_point,
                last_update, capacity,
                node_policy_time_lock_delta, node_policy_min_htlc, node_policy_fee_base_msat, node_policy_fee_rate_milli_msat,
                extra_created) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)'''

    sql_update = '''UPDATE describegraph_channels SET last_update=?, node_policy_time_lock_delta=?,
                node_policy_min_htlc=?, node_policy_fee_base_msat=?, node_policy_fee_rate_milli_msat=?
                WHERE channel_id=? AND node_pub=?'''

    sql_insert_history = '''INSERT INTO describegraph_channels_policy_history (channel_id, node_pub, timestamp,
                node_policy_time_lock_delta, node_policy_min_htlc, node_policy_fee_base_msat, node_policy_fee_rate_milli_msat)
                VALUES (?, ?, ?, ?, ?, ?, ?)'''

    to_insert = []
    to_update = []
    insert_history = []
    known_channels = known.keys()
    for ch in dg.edges:
        policies1 = (ch.node1_policy.time_lock_delta, ch.node1_policy.min_htlc,
            ch.node1_policy.fee_base_msat, ch.node1_policy.fee_rate_milli_msat)
        policies2 = (ch.node2_policy.time_lock_delta, ch.node2_policy.min_htlc,
            ch.node2_policy.fee_base_msat, ch.node2_policy.fee_rate_milli_msat)

        if (ch.channel_id, ch.node1_pub) in known_channels:
            to_update.append((ch.last_update,) + policies1 + (ch.channel_id, ch.node1_pub))
            to_update.append((ch.last_update,) + policies2 + (ch.channel_id, ch.node2_pub))

            if policies1 != known[(ch.channel_id, ch.node1_pub)]:
                # Policy of the channel changed, add to history
                print("Policy of channel %d, node %s changed:" % (ch.channel_id, ch.node1_pub[:16]),
                        known[(ch.channel_id, ch.node1_pub)], policies1)
                insert_history.append((ch.channel_id, ch.node1_pub, timestamp) + policies1)

            if policies2 != known[(ch.channel_id, ch.node2_pub)]:
                # Policy of the channel changed, add to history
                print("Policy of channel %d, node %s changed:" % (ch.channel_id, ch.node2_pub[:16]),
                        known[(ch.channel_id, ch.node2_pub)], policies2)
                insert_history.append((ch.channel_id, ch.node2_pub, timestamp) + policies2)

        else:
            # FIXME: Detect channel creation date from founding tx
            extra_created = 11111

            to_insert.append((ch.channel_id, ch.node1_pub, ch.chan_point, ch.last_update,
                ch.capacity,) + policies1 + (extra_created,))
            to_insert.append((ch.channel_id, ch.node2_pub, ch.chan_point, ch.last_update,
                ch.capacity,) + policies2 + (extra_created,))

            insert_history.append((ch.channel_id, ch.node1_pub, timestamp) + policies1)
            insert_history.append((ch.channel_id, ch.node2_pub, timestamp) + policies2)

    print("New %d channels..." % len(to_insert))
    db.executemany(sql_insert, to_insert)
    print("Updating %d channels..." % len(to_update))
    db.executemany(sql_update, to_update)
    print("Inserting %d histories..." % len(insert_history))
    db.executemany(sql_insert_history, insert_history)
    db.commit()

def update_listchannels(db, channels, transactions, timestamp):
    print("Updating listchannels...")

    print("Fetching channels...")
    known = {}
    last_active = {}
    for row in db.execute('''SELECT channel_point, active, local_balance, remote_balance,
                total_satoshis_sent, total_satoshis_received, commit_fee, extra_last_update FROM channels'''):
        known[row['channel_point']] = (row['active'], row['local_balance'], row['remote_balance'],
                row['total_satoshis_sent'], row['total_satoshis_received'], row['commit_fee'])
        last_active[row['channel_point']] = row['extra_last_update']

    # Fetch aliases of recently active nodes
    print("Fetching aliases...")
    aliases = {}
    for row in db.execute("SELECT pub_key, alias FROM describegraph_nodes"):
        aliases[row['pub_key']] = row['alias']

    sql_insert = '''INSERT INTO channels (channel_point, chan_id, active, remote_pubkey,
                capacity, local_balance, remote_balance, commit_fee, commit_weight, fee_per_kw,
                unsettled_balance, total_satoshis_sent, total_satoshis_received, num_updates,
                csv_delay, private, extra_is_mine, extra_created, extra_last_update, remote_alias) VALUES
                (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)'''

    sql_update = '''UPDATE channels SET active=?, local_balance=?, remote_balance=?,
                total_satoshis_sent=?, total_satoshis_received=?, commit_fee=?,
                unsettled_balance=?, num_updates=?, extra_last_update=?, remote_alias=?
                WHERE channel_point=?'''

    sql_insert_history = '''INSERT INTO channels_history (channel_point, timestamp, active,
                local_balance, remote_balance, total_satoshis_sent, total_satoshis_received, commit_fee)
                VALUES (?, ?, ?, ?, ?, ?, ?, ?)'''

    to_insert = []
    to_update = []
    insert_history = []
    known_channels = known.keys()
    for ch in channels.channels:
        variables = (ch.active, ch.local_balance, ch.remote_balance, ch.total_satoshis_sent, ch.total_satoshis_received, ch.commit_fee)
        alias = aliases.get(ch.remote_pubkey, '') or ch.remote_pubkey[:10]

        if ch.channel_point in known_channels:

            # Update channel extra_last_update if channel is active
            if ch.active:
               extra_last_update = timestamp
            else:
                extra_last_update = last_active.get(ch.channel_point, 0)

            if ch.active != known[ch.channel_point][0]:
                print("Channel %s changed state to active:" % ch.channel_point, ch.active)

            to_update.append(variables + (ch.unsettled_balance, ch.num_updates, extra_last_update, alias, ch.channel_point,))

            if known[ch.channel_point] != variables:
                print(ch.channel_point, known[ch.channel_point], variables)
                if known[ch.channel_point] != variables:
                    insert_history.append((ch.channel_point, timestamp,) + variables)

        else:
            (extra_is_mine, extra_created) = lookup_tx(ch.channel_point, transactions)
            extra_last_update = extra_created

            to_insert.append((ch.channel_point, ch.chan_id, ch.active, ch.remote_pubkey,
                ch.capacity, ch.local_balance, ch.remote_balance, ch.commit_fee, ch.commit_weight,
                ch.fee_per_kw, ch.unsettled_balance, ch.total_satoshis_sent, ch.total_satoshis_received,
                ch.num_updates, ch.csv_delay, ch.private,
                extra_is_mine, extra_created, extra_last_update, alias))

            insert_history.append((ch.channel_point, timestamp,) + variables)

    print("New %d channels..." % len(to_insert))
    db.executemany(sql_insert, to_insert)
    print("Updating %d channels..." % len(to_update))
    db.executemany(sql_update, to_update)
    print("Inserting %d histories..." % len(insert_history))
    db.executemany(sql_insert_history, insert_history)
    db.commit()

def update_closedchannels(db, closedchannels, timestamp):
    print("Updating closed channels...")

    sql_update = '''UPDATE channels SET active=-1, close_tx_hash=?, close_height=?, close_settled_balance=?,
                        close_type=?, close_time_locked_balance=?, close_timestamp=?
                    WHERE channel_point=?'''

    closure_type = {}
    for value, key in ln.ChannelCloseSummary.ClosureType.items():
        closure_type[key] = value

    print("Fetching channels...")
    closed = {}
    for row in db.execute('''SELECT channel_point, active FROM channels'''):
        closed[row['channel_point']] = True if row['active'] == -1 else False

    to_update = []
    for ch in closedchannels.channels:
        if ch.channel_point not in closed.keys():
            # Channel is unkown to our channel db, skipping
            continue
        elif closed[ch.channel_point] == True:
            # Channel is already known as closed
            continue

        to_update.append((ch.closing_tx_hash, ch.close_height, ch.settled_balance,
                closure_type[ch.close_type], ch.time_locked_balance, timestamp, ch.channel_point))

    print("Closing %d channels" % len(to_update))
    db.executemany(sql_update, to_update)

def get_forwardinghistory_items(db):
    for row in db.execute('SELECT COUNT(*) as count FROM fwdinghistory'):
        print("Records in fwdinghistory: %d" % row['count'])
        return row['count']

def update_forwardinghistory(db, history):
    print("Updating forwarding history...")
    sql = '''INSERT INTO fwdinghistory (chan_id_in, chan_id_out, timestamp,
                amt_in, amt_out, fee) VALUES (?, ?, ?, ?, ?, ?)'''

    to_insert = []
    for event in history.forwarding_events:
        to_insert.append((event.chan_id_in, event.chan_id_out, event.timestamp,
            event.amt_in, event.amt_out, event.fee))

    print("New %d events..." % len(to_insert))
    db.executemany(sql, to_insert)
    db.commit()

def update_balances(db, walletbalance, channelbalance, timestamp):

    print("Updating balances...")
    sql = '''INSERT INTO balances (timestamp, wallet_total, wallet_confirmed,
            wallet_unconfirmed, channel_total, channel_pending) VALUES
            (?, ?, ?, ?, ?, ?)'''

    print("wallet_total", walletbalance.total_balance)
    print("wallet_confirmed", walletbalance.confirmed_balance)
    print("wallet_unconfirmed", walletbalance.unconfirmed_balance)
    print("channel_balance", channelbalance.balance)
    print("channel_pending", channelbalance.pending_open_balance)

    db.execute(sql, (timestamp, walletbalance.total_balance, walletbalance.confirmed_balance,
        walletbalance.unconfirmed_balance, channelbalance.balance, channelbalance.pending_open_balance))
    db.commit()

def collect(args, db, rpc):
    start = int(time.time())

    networkinfo = rpc.GetNetworkInfo(ln.NetworkInfoRequest())
    update_networkinfo(db, networkinfo, start)

    describegraph = rpc.DescribeGraph(ln.ChannelGraphRequest())
    update_describegraph_nodes(db, describegraph, start)
    update_describegraph_channels(db, describegraph, start)

    listchannels = rpc.ListChannels(ln.ListChannelsRequest())
    transactions = rpc.GetTransactions(ln.GetTransactionsRequest())
    update_listchannels(db, listchannels, transactions, start)

    forwardinghistory = rpc.ForwardingHistory(ln.ForwardingHistoryRequest(
        start_time=0,
        end_time=start,
        index_offset=get_forwardinghistory_items(db),
        num_max_events=50000))
    update_forwardinghistory(db, forwardinghistory)

    closedchannels = rpc.ClosedChannels(ln.ClosedChannelsRequest())
    update_closedchannels(db, closedchannels, start)

    walletbalance = rpc.WalletBalance(ln.WalletBalanceRequest())
    channelbalance = rpc.ChannelBalance(ln.ChannelBalanceRequest())
    update_balances(db, walletbalance, channelbalance, start)

    # FIXME pendingchannels
